require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
  
  def setup
    @user = users(:michael)
    remember(@user)
  end
  
  # app/helpers/sessions_helper.rb#current_user で、
  # セッションが使用されておらず、クッキーが使用されている場合をテストする
  # セッションを使用しないために、フィクスチャからuser変数を定義している
  test "current_user returns right user when session is nil" do
    assert_equal @user, current_user
    assert is_logged_in?
  end
  
  test "current_user returns nil when remember digest is wrong" do
    # remember_digestをnew_tokenのdigestで更新し、
    # remember_tokenとremember_digestの不一致を発生させる
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end
