require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    # deliveries配列はグローバルなので、
    # 並行して行われる他のテストでメールが配信された場合に備えて初期化する。
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: {
        name:                  "",
        email:                 "user@invalid",
        password:              "foo",
        password_confirmation: "bar" }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.alert'
    assert_select 'div.alert-danger'
    # nameのブランク、emailのフォーマットエラー、パスワードの長さ不足、
    # パスワードと確認用パスワードの不一致で
    # 4件のメッセージが表示されることを確認
    assert_select 'div#error_explanation ul li', 4
  end
  
  test "valid signup information with account activation" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user: {
        name:                  "Example User",
        email:                 "user@example.com",
        password:              "password",
        password_confirmation: "password" }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # 有効化していない状態でログインした場合、未ログイン状態であること
    log_in_as(user)
    assert_not is_logged_in?
    # 有効化トークンが不正な場合、未ログイン状態であること
    get edit_account_activation_path("invalid token")
    assert_not is_logged_in?
    # トークンは正しいがメールアドレスが無効な場合、未ログイン状態であること
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # 有効化トークンが正しい場合、userが有効化され、ログイン状態であること
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
    assert_not flash.empty?
  end
end
