require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end
  
  test "password resets" do
    # 「forgot password」フォーム
    get new_password_reset_path
    assert_template 'password_resets/new/'
    
    # メールアドレスが無効な場合、
    # 警告と共にメールアドレスの入力画面を再表示すること
    post password_resets_path, password_reset: { email: "" }
    assert_not flash.empty?
    assert_template 'password_resets/new/'
    
    # メールアドレスが有効な場合、リセットダイジェストを更新し、
    # パスワード再設定メールを送信し、送信メッセージと共にホームを表示すること
    post password_resets_path, password_reset: { email: @user.email }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    
    
    # パスワード再設定用フォーム
    user = assigns(:user)
    
    # メールアドレスが無効な場合、ホームを表示すること
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url

    # ユーザーが無効な場合、ホームを表示すること
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url
    user.toggle!(:activated)
    
    # トークンが無効な場合、ホームを表示すること
    get edit_password_reset_path('wrong token', email: user.email)
    assert_redirected_to root_url
    
    # メールアドレスもトークンも有効な場合、emailのhiddenのinput要素を作成し、
    # パスワード再設定用フォームを表示すること
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email
    
    # 無効なパスワードと確認の場合、警告を表示すること
    patch password_reset_path(user.reset_token),
            email: user.email,
            user: {
              password:              "foobaz",
              password_confirmation: "barquux" }
    assert_select 'div#error_explanation', 1
    
    # パスワードが空の場合、警告を表示すること
    patch password_reset_path(user.reset_token),
            email: user.email,
            user: {
              password:              "",
              password_confirmation: "" }
    assert_select 'div#error_explanation', 1
    
    # 有効なパスワードと確認の場合、ログインし、ユーザーのshowページに遷移し、
    # パスワードリセットの完了メッセージを表示すること
    patch password_reset_path(user.reset_token),
            email: user.email,
            user: {
              password:              "foobaz",
              password_confirmation: "foobaz" }
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to user
  end
end
